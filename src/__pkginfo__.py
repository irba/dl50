# **************************************************************************
#
# inspigtor
#
# **************************************************************************

__description__ = "Determination of Dl50"

__long_description__ = """dl50 is a software for determining the DL50 based on the OECD recommendations.
"""

__author__ = "E.C. Pellegrini"

__author_email__ = "ericpellegrini76@gmail.com"

__maintainer__ = "E.C. Pellegrini"

__maintainer_email__ = "ericpellegrini76@gmail.com"

__repo__ = "https://gitlab.com/eurydice38/dl50.git"

__license__ = "GPL 3"

__version__ = "0.0.0"
